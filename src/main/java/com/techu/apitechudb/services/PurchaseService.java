package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.util.BsonUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;
    @Autowired
    UserService userService;
    @Autowired
    ProductService productService;

    private Optional<PurchaseModel> findById(String id){
        System.out.println("findById en UserService");

        return this.purchaseRepository.findById(id);
    }

    public String add(PurchaseModel purchase){
        System.out.println("add en PurchaseModel");

        String result = "";
        float resultAmount = 0;

        Optional<UserModel> resultUser = this.userService.findById(purchase.getUserId());
//      Optional<ProductModel> resultProduct = this.productService.findAll(purchase.getId());

        if (!resultUser.isPresent()){
            System.out.println("Usuario NO encontrado, cancelando compra...");
            result = "Usuario NO encontrado, cancelando compra";
        }else{
            System.out.println("Usuario encontrado");
        }

        if (this.findById(purchase.getId()).isPresent()) {
            System.out.println("Compra ya realizada, cancelando compra...");
            result = "Compra ya realizada, cancelando compra";
        }

        //        for (ProductModel product : this.productRepository.findAll() ){
//            product.getId();
//        }

//      for (PurchaseModel purchaseModel : resultProduct){
//          product.getId();
//      }


        if (result.equals("")){
            this.purchaseRepository.save(purchase);
        }

        return result;
    }

    public List<PurchaseModel> findAll(){
        System.out.println("findAll en PurchaseService");

        return this.purchaseRepository.findAll();
    }


}
