package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

//ORM

//UPSERT -> viene por usar la funcion save de mongo, si se usase INSERT entonces al
//insertar dos veces daría alguna clase de error, duplicado o el que fuese.

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll(){
        System.out.println("findAll en ProductService");

        return this.productRepository.findAll();
    }

    //Se usa el optional para evitar el devolver un productmodel nulo si no se encuentra nada
    public Optional<ProductModel> findById(String id){
        System.out.println("findById en ProductService");

        //Otra opcion
//        for (ProductModel product : this.productRepository.findAll() ){
//            product.getId();
//        }
        return this.productRepository.findById(id);
    }

    public ProductModel add(ProductModel product){
        System.out.println("add en ProductService");
//aqui se meterian validaciones previas y/o cosas necesarias antes de grabar en la BBDD
//      return this.productRepository.insert(product);
        return this.productRepository.save(product);
    }

    public ProductModel update(ProductModel productModel) {
        System.out.println("update en ProductService");

        return this.productRepository.save(productModel);

    }

    public boolean delete(String id) {
        System.out.println("delete en ProducService");
        boolean result = false;

        if (this.findById(id).isPresent() == true){
            System.out.println("Producto encontrado, borrando");

            this.productRepository.deleteById(id);
            result = true;
        }

        return result;
    }
}
