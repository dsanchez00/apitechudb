package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> getUsers(String sort){
        System.out.println("getUsers en UserService");

        List<UserModel> result;

//      if (orderBy != null){

        if (sort.equals("Y")){
//            return this.userRepository.findAll(Sort.by(Sort.Direction.ASC, "age"));
//este sort tambien funciona
//            result = this.userRepository.findAll(Sort.by(Sort.Direction.ASC, "age"));
            result = this.userRepository.findAll(Sort.by("age"));
        }else{
//            return this.userRepository.findAll();
            result = this.userRepository.findAll();
        }

        return result;
    }

    public Optional<UserModel> findById(String id){
        System.out.println("findById en UserService");

        return this.userRepository.findById(id);
    }

    public boolean add(UserModel user){
        System.out.println("add en UserService");
        boolean result = false;

        if (!this.findById(user.getId()).isPresent()){
            System.out.println("Usuario no encontrado, insertando");

            this.userRepository.save(user);
            result = true;
        }

        return result;
    }

    public boolean update(UserModel user, String id) {
        System.out.println("update en UserService");
        boolean result = false;

        if (this.findById(id).isPresent()){
            System.out.println("Usuario encontrado, actualizando");

            user.setId(id);
            this.userRepository.save(user);
            result = true;
        }

        return result;
    }

    public boolean delete(String id) {
        System.out.println("delete en UserService");
        boolean result = false;

        if (this.findById(id).isPresent() == true){
            System.out.println("Usuario encontrado, borrando");

            this.userRepository.deleteById(id);
            result = true;
        }

        return result;
    }

}
