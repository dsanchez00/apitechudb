package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.services.PurchaseService;
import com.techu.apitechudb.services.PurchaseServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apitechu/v2/")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @GetMapping("/purchases/prueba")
    public ResponseEntity<String> prueba(){
        System.out.println("----prueba");

        return new ResponseEntity<>(
                "SALIDA",
                HttpStatus.OK
        );
    }

    @PostMapping("/purchases")
    public PurchaseServiceResponse addPurchase(@RequestBody PurchaseModel purchase){

//    public ResponseEntity<String> addPurchase(@RequestBody PurchaseModel purchase){
        System.out.println("----addPurchase");
        System.out.println("La id de la compra es " + purchase.getId());
        System.out.println("La id del usuario en compra es " + purchase.getUserId());
        System.out.println("El total de la compra es " + purchase.getAmount());
        System.out.println("Los productos y cantidades de la compra son " + purchase.getPurchaseItems());

        String addPurchase = this.purchaseService.add(purchase);

        return new ResponseEntity<>(
                addPurchase.equals("") ? "Compra realizada" : addPurchase ,
                addPurchase.equals("") ? HttpStatus.OK : HttpStatus.BAD_REQUEST
        );
    }

    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases(){
        System.out.println("----getPurchases");

        return new ResponseEntity<>(
                this.purchaseService.findAll(),
                HttpStatus.OK
        );
    }

}
