package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2/")

//El CrossOrigin es para el CORS,  esto no nos va a salir a backend sino en el navegador (frontend)
//Esto no se puede solucionar desde frontend, sino desde backend con esto.
//Origins son los servidores desde donde se permite hacer la conexion, * es todos, es solo de prueba
//methods son los metodos que se permiten
//Tambien se puede poner a nivel de métodos (en el getUsers, etc)
//@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(
//Otra forma de recuperar parametros opcionales:
//            @RequestParam(name = "$orderby", required = false) String orderBy){
            @RequestParam(value = "sort", defaultValue = "N") String sort){
        System.out.println("----getUsers");

        if (sort.equals("Y")){
            System.out.println("Es Y");
        }else{
            System.out.println("Es N");
        }

        return new ResponseEntity<>(
                this.userService.getUsers(sort),
                HttpStatus.OK
        );
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getuserById(@PathVariable String id){
        System.out.println("----getuserById");
        System.out.println("El id del usuario a buscar es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado" ,
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/users")
    public ResponseEntity<String> addUser(@RequestBody UserModel user){
        System.out.println("----addUser");
        System.out.println("La id del usuario a crear es " + user.getId());
        System.out.println("El nombre del usuario a crear es " + user.getName());
        System.out.println("La edad del usuario a crear es " + user.getAge());

        boolean addUser = this.userService.add(user);

        return new ResponseEntity<>(
                addUser ? "Usuario insertado" : "Usuario ya existía" ,
                addUser ? HttpStatus.OK : HttpStatus.BAD_REQUEST
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<String> updateUser (
            @RequestBody UserModel user, @PathVariable String id
    ){
        System.out.println("----updateUser");
        System.out.println("El id del usuario a actualizar en parametro URL es " + id);
        System.out.println("El id del usuario por body (que no se va a usar) es " + user.getId());
        System.out.println("El nombre del usuario a actualizar es " + user.getName());
        System.out.println("La edad del usuario a actualizar es " + user.getAge());

        boolean updateProduct = this.userService.update(user,id);

        return new ResponseEntity<>(
                updateProduct ? "Usuario actualizado" : "Usuario no encontrado" ,
                updateProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){
        System.out.println("----deleteUser");
        System.out.println("El id del usuario a borrar es " + id);

        boolean deleteProduct = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteProduct ? "Usuario borrado" : "Usuario no encontrado" ,
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }
}
