package com.techu.apitechudb.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.Map;

@Document(collection = "purchases")
public class PurchaseModel {

    @Id
    private String id;
    private String userId;
    private float amount;
//    private Map purchaseItems;
    private Map<Object, Object> purchaseItems = new HashMap<Object, Object>();

    public PurchaseModel() {
    }

    public PurchaseModel(String id, String userId, float amount, Map purchaseItems) {
        this.id = id;
        this.userId = userId;
        this.amount = amount;
        this.purchaseItems = purchaseItems;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public float getAmount() {
        return this.amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public Map<Object, Object> getPurchaseItems() {
        return this.purchaseItems;
    }

    public void setPurchaseItems(Map<Object, Object> purchaseItems) {
        this.purchaseItems = purchaseItems;
    }

}
